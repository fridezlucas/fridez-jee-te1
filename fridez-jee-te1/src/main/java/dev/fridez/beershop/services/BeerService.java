package dev.fridez.beershop.services;

import java.util.Collection;

import dev.fridez.beershop.models.Beer;

/**
 * BeerService Interface Allows developer to use multiple beers service with
 * this one
 * 
 * @author Lucas Fridez <lucas.fridez@he-arc.ch>
 */
public interface BeerService {

	/**
	 * @return Collection of stored Beer
	 */
	public Collection<Beer> getBeers();

	/**
	 * Get a Beer by id
	 * 
	 * @param id Beer id
	 * @return Beer
	 */
	public Beer getBeer(int id);

	/**
	 * Create a Beer and store it
	 * 
	 * @param beer Beer to store
	 * @return true if Beer correctly stored; false otherwise
	 */
	public boolean createBeer(Beer beer);

	/**
	 * Update a Beer by id
	 * 
	 * @param id   Beer id
	 * @param beer Beer to update
	 * @return true if Beer correctly updated; false otherwise
	 */
	public boolean updateBeer(int id, Beer beer);

	/**
	 * Delete a Beer by Id
	 * 
	 * @param id Beer id
	 * @return true if Beer correctly deleted; false otherwise
	 */
	public boolean deleteBeer(int id);

}
