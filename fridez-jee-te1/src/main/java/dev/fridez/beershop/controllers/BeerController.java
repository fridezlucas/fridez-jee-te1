package dev.fridez.beershop.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dev.fridez.beershop.db.VolatileBeerDb;
import dev.fridez.beershop.models.Beer;
import dev.fridez.beershop.models.Response;
import dev.fridez.beershop.services.BeerService;
import dev.fridez.beershop.validators.BeerValidator;

/**
 * Beer API Controller
 * 
 * @author Lucas Fridez <lucas.fridez@he-arc.ch>
 *
 */
@RestController
@RequestMapping("/beers")
public class BeerController {
	
	/**
	 * String (to format) that say 'Beer no exists'
	 */
	private static final String BEER_ID_NOT_FOUND = "Beer with id %d does not exist !";

	/**
	 * Beer Service
	 */
	private BeerService beerService;

	/**
	 * Launch Beer Controller at application startup
	 */
	public BeerController() {
		this.beerService = VolatileBeerDb.getInstance();
	}

	/**
	 * Get all beers from service
	 * 
	 * @return Collection of Beers
	 */
	@GetMapping
	@ResponseBody
	public Collection<Beer> getAllBeers() {
		return beerService.getBeers();
	}

	/**
	 * Get a Beer by its id
	 * 
	 * @param id Beer id
	 * @return Api Response
	 */
	@GetMapping("/{id}")
	public Response getBeerById(@PathVariable("id") int id) {
		List<String> errors = new ArrayList<>();
		Beer beer = beerService.getBeer(id);

		if (beer == null) {
			errors.add(String.format(BEER_ID_NOT_FOUND, id));
			return new Response("Error occured on Beer recuperation", errors);
		}
		return new Response("Beer successfully found !", beer);
	}

	/**
	 * Create a Beer
	 * 
	 * @param beer to save
	 * @return Api Response
	 */
	@PostMapping(consumes = "application/json", produces = "application/json")
	public Response postBeer(@RequestBody Beer beer) {
		List<String> errors = BeerValidator.validate(beer);

		if (errors.isEmpty() && beerService.createBeer(beer)) {
			return new Response("Beer successfully created !");
		}

		return new Response("Error occured on Beer creation", errors);

	}

	/**
	 * Update a Beer
	 * 
	 * @param id   Beer id to update
	 * @param beer Updated Beer information
	 * @return Api Response
	 */
	@PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
	public Response putBeer(@PathVariable("id") int id, @RequestBody Beer beer) {
		List<String> errors = BeerValidator.validate(beer);

		if (beerService.getBeer(id) == null) {
			errors.add(String.format(BEER_ID_NOT_FOUND, id));
		}

		if (errors.isEmpty() && beerService.updateBeer(id, beer)) {
			return new Response("Beer successfully updated !");
		}

		return new Response("Error occured on Beer update", errors);
	}

	/**
	 * Delete a Beer by id
	 * 
	 * @param id Beer id
	 * @return Api Response
	 */
	@DeleteMapping("/{id}")
	public Response deleteBeerById(@PathVariable("id") int id) {
		List<String> errors = new ArrayList<>();

		if (beerService.getBeer(id) == null) {
			errors.add(String.format(BEER_ID_NOT_FOUND, id));
			return new Response("Error occured on Beer deletion", errors);
		}
		beerService.deleteBeer(id);
		return new Response("Beer successfully deleted !");
	}
}
