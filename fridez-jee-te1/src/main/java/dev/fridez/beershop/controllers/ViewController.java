package dev.fridez.beershop.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import dev.fridez.beershop.db.VolatileBeerDb;
import dev.fridez.beershop.models.Beer;
import dev.fridez.beershop.services.BeerService;
import dev.fridez.beershop.validators.BeerValidator;

/**
 * ViewController
 * @author Lucas Fridez <lucas.fridez@he-arc.ch>
 *
 */
@Controller
@RequestMapping("/app")
public class ViewController {
	
	private static final String REDIRECT_LIST_BEERS = "redirect:/app/beers";

	/**
	 * Beer Service
	 */
	private BeerService beerService;

	/**
	 * Instantiate ViewController and its volatile database
	 */
	public ViewController() {
		this.beerService = VolatileBeerDb.getInstance();
	}

	/**
	 * Add validation attributes in model
	 * @param model model to add attributes
	 */
	private void setValidation(Model model) {
		model.addAttribute("MIN_NAME_LENGTH", BeerValidator.MIN_NAME_LENGTH);
		model.addAttribute("MIN_YEAR", BeerValidator.MIN_YEAR);
		model.addAttribute("MAX_YEAR", BeerValidator.MAX_YEAR);
	}

	/**
	 * HHandle <null>, / and /index requests
	 * @param model model for view
	 * @return index view
	 */
	@RequestMapping(value = { "", "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model) {

		return "pages/index";
	}

	/**
	 * Handle GET /beers request
	 * @param model model for view
	 * @return list of beers view
	 */
	@RequestMapping(value = { "/beers" }, method = RequestMethod.GET)
	public String listBeersView(Model model) {

		model.addAttribute("beers", beerService.getBeers());

		return "pages/listBeers";
	}

	/**
	 * HAndle /beers/add view
	 * @param model model for view
	 * @return page to add a Beer
	 */
	@RequestMapping(value = { "/beers/add" }, method = RequestMethod.GET)
	public String addBeerView(Model model) {
		setValidation(model);
		return "pages/addBeer";
	}

	/**
	 * Save a Beer
	 * @param model model for view
	 * @param name name for beer to create
	 * @param year year for beer to create
	 * @return redirection to list of beers
	 */
	@RequestMapping(value = { "/beers/addBeer" }, method = RequestMethod.POST)
	public String addBeer(Model model, @RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "year", required = true) int year) {

		beerService.createBeer(new Beer(0, name, year));
		return REDIRECT_LIST_BEERS;
	}

	/**
	 * Handle /beers/edit/{id} request
	 * @param model model for view
	 * @param id id of beer to edit
	 * @return edit Beer page 
	 */
	@RequestMapping(value = { "/beers/edit/{id}" }, method = RequestMethod.GET)
	public String editBeerView(Model model, @PathVariable("id") int id) {

		setValidation(model);
		model.addAttribute("id", id);
		model.addAttribute("beer", beerService.getBeer(id));

		return "pages/editBeer";
	}

	/**
	 * Edit a Beer
	 * @param model model for view
	 * @param beer beer to replace in database
	 * @return redirection to list of beers
	 */
	@RequestMapping(value = { "/beers/editBeer" }, method = RequestMethod.POST)
	public String editBeer(Model model, @ModelAttribute("beer") Beer beer) {

		beerService.updateBeer(beer.getId(), beer);

		return REDIRECT_LIST_BEERS;
	}

	/**
	 * Handle /beers/delete/{id} request
	 * @param model model for view
	 * @param id id of beer to delete
	 * @return redirection to list of beers
	 */
	@RequestMapping(value = { "/beers/delete/{id}" }, method = RequestMethod.GET)
	public String deleteBeerView(Model model, @PathVariable("id") int id) {
		beerService.deleteBeer(id);
		return REDIRECT_LIST_BEERS;
	}
}
