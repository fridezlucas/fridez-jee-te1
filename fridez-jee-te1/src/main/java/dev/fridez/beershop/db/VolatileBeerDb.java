package dev.fridez.beershop.db;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import dev.fridez.beershop.models.Beer;
import dev.fridez.beershop.services.BeerService;

/**
 * VolatileBeer Database Store beers in a Map to use a BeerService without
 * database
 * 
 * @author Lucas Fridez <lucas.fridez@he-arc.ch>
 *
 */
public class VolatileBeerDb implements BeerService {

	/**
	 * Beer Map to store beers (like a database)
	 */
	private Map<Integer, Beer> beerDb;

	/**
	 * Singleton property
	 */
	private static VolatileBeerDb instance = null;
	
	/**
	 * Next id property
	 */
	private int nextId = 1;

	/**
	 * Instantiate a Beer Database Insert some default beers
	 */
	private VolatileBeerDb() {
		super();
		this.beerDb = new HashMap<>();

		createBeer(new Beer(1, "West Coast Trip", 2018));
		createBeer(new Beer(1, "Owly Crap", 2018));
		createBeer(new Beer(1, "Wanderer", 2019));
	}

	/**
	 * @return Singleton Database instance
	 */
	public static VolatileBeerDb getInstance() {
		if (instance == null) {
			instance = new VolatileBeerDb();
		}
		return instance;
	}

	/**
	 * Get all Beers from map
	 * 
	 * @return Beers Collection
	 */
	@Override
	public Collection<Beer> getBeers() {
		return this.beerDb.values();
	}

	/**
	 * Get a Beer
	 * 
	 * @param id Beer id
	 * @return Beer corresponding to given id if exists; null otherwise
	 */
	@Override
	public Beer getBeer(int id) {
		if (this.beerDb.containsKey(id)) {
			return this.beerDb.get(id);
		}
		return null;
	}

	/**
	 * Create a Beer
	 * 
	 * @param Beer Beer to insert
	 * @return true if Beer is inserted; false otherwise
	 */
	@Override
	public boolean createBeer(Beer beer) {

		try {
			int id = nextId;
			beer.setId(id);
			this.beerDb.put(id, beer);
			nextId++;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Update a Beer
	 * 
	 * @param id   Beer id to update
	 * @param Beer Beer object to replace current Beer
	 * @return true if Beer correctly updated; false otherwise
	 */
	@Override
	public boolean updateBeer(int id, Beer beer) {
		try {
			beer.setId(id); // Avoid id modification
			this.beerDb.put(id, beer);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete a Beer
	 * 
	 * @param id Beer id to update
	 * @return true if Beer correctly deleted; false otherwise
	 */
	@Override
	public boolean deleteBeer(int id) {
		try {
			this.beerDb.remove(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
