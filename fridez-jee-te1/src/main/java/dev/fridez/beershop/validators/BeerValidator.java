package dev.fridez.beershop.validators;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import dev.fridez.beershop.models.Beer;

/**
 * Beer Validator
 * 
 * @author Lucas Fridez <lucas.fridez@he-arc.ch>
 *
 */
public final class BeerValidator {
	
	/**
	 * Hide public constructor to write a complete static class
	 */
	private BeerValidator() {
		
	}

	/**
	 * Min name length
	 */
	public static final int MIN_NAME_LENGTH = 2;

	/**
	 * Min year for a Beer creation/update
	 */
	public static final int MIN_YEAR = 1000;

	/**
	 * Max year for a Beer creation/update
	 */
	public static final int MAX_YEAR = Calendar.getInstance().get(Calendar.YEAR);

	/**
	 * Validate a Beer
	 * @param beer beer to validate
	 * @return true if beer is valid; false otherwise
	 */
	public static final List<String> validate(Beer beer) {
		List<String> errors = new ArrayList<>();

		if (!validateName(beer.getName())) {
			errors.add(String.format("Name must be a string with at least %d characters.", MIN_NAME_LENGTH));
		}
		if (!validateYear(beer.getYear())) {
			errors.add(String.format("Year must be between %d and %d.", MIN_YEAR, MAX_YEAR));
		}

		return errors;
	}

	/**
	 * Validate Beer name
	 * @param name Beer name
	 * @return true if name is valid; false otherwise
	 */
	private static final boolean validateName(String name) {
		return name.length() >= MIN_NAME_LENGTH;
	}

	/**
	 * Validate a Beer Year
	 * @param year Beer Year
	 * @return true if year is valid; false otherwise
	 */
	private static final boolean validateYear(int year) {
		return year >= MIN_YEAR && year <= MAX_YEAR;
	}
}
