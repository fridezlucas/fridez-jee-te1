package dev.fridez.beershop.models;

/**
 * Beer Pojo
 * 
 * @author Lucas Fridez <lucas.fridez@he-arc.ch>
 *
 */
public class Beer {
	/**
	 * Beer Id
	 */
	private int id;

	/**
	 * Beer name
	 */
	private String name;

	/**
	 * Beer creation year
	 */
	private int year;

	/**
	 * Instantiate a new Beer
	 * 
	 * @param id   Beer id
	 * @param name Beer name
	 * @param year Beer creation year
	 */
	public Beer(int id, String name, int year) {
		this.id = id;
		this.name = name;
		this.year = year;
	}

	/**
	 * @return Beer Id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Change Beer Id
	 * 
	 * @param id New id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return Beer name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Change Beer name
	 * 
	 * @param name Name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Beer creation year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * Change beer creation year
	 * 
	 * @param year Beer creation year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * String representation of a Beer
	 */
	@Override
	public String toString() {
		return "Beer [id=" + id + ", name=" + name + ", year=" + year + "]";
	}

}
