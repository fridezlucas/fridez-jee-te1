package dev.fridez.beershop.models;

import java.util.List;

/**
 * Api Response Pojo class
 * 
 * @author Lucas Fridez <lucas.fridez@he-arc.ch>
 *
 */
public class Response {

	/**
	 * Message field
	 */
	private String message;

	/**
	 * Code field
	 */
	private int code;

	/**
	 * Errors field
	 */
	private List<String> errors;

	/**
	 * Data field
	 */
	private Object data;

	/**
	 * Instantiate a new Response without any errors and data
	 * 
	 * @param message message to send
	 */
	public Response(String message) {
		this.message = message;
		this.code = 200;
		this.errors = null;
		this.data = null;
	}

	/**
	 * Instantiate a new Reponse with errors
	 * 
	 * @param message message to send
	 * @param errors  errors to send
	 */
	public Response(String message, List<String> errors) {
		this.message = message;
		this.code = 500;
		this.errors = errors;
		this.data = null;
	}

	/**
	 * Instantiate a new Response with data
	 * 
	 * @param message message to send
	 * @param data    data to send
	 */
	public Response(String message, Object data) {
		this.message = message;
		this.code = 200;
		this.data = data;
		this.errors = null;
	}

	/**
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Update message
	 * 
	 * @param message message to send
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Update code
	 * 
	 * @param code code to send
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * @return errors list
	 */
	public List<String> getErrors() {
		return errors;
	}

	/**
	 * Uodate errors
	 * 
	 * @param errors errors list to send
	 */
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	/**
	 * @return data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * Update data
	 * 
	 * @param data data to send
	 */
	public void setData(Object data) {
		this.data = data;
	}
}
