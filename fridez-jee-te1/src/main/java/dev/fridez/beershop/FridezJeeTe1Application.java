package dev.fridez.beershop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TE1 Application
 * 
 * @author Lucas Fridez <lucas.fridez@he-arc.ch>
 *
 */
@SpringBootApplication
public class FridezJeeTe1Application {

	/**
	 * Run Spring application
	 * 
	 * @param args arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(FridezJeeTe1Application.class, args);
	}

}
