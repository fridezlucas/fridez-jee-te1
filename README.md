# Fridez JEE TE1

Ce projet prend part à l'évaluation (1er TE) du cours **JEE** enseigné par M. Chèvre.  
Ce cours prend part à la 3ème année du Bachelor Of Sciences en Informatique Orientation Développement Logiciel et Multimédia, enseigné à la HE-Arc Ingénierie.

## Description

Création d'un projet SpringBoot/Thymeleaf, permettant la gestion de bières, comptant pour 1/3 du TE de janvier.  
Le développement est à rendre le 16 janvier.

## Fonctionnalités

- Saisie de bières
- Modification/Suppression de bières
- Liste des bières

### Phase 1

- Création d'un projet SpringBoot, avec dépendances web et thymeleaf.
- Pas de bases de données, les bières sont stockées en mémoire (List, HashMap, etc...)
- Création d'un endpoint REST /bieres
    - `GET /bieres` --> Liste des bières
    - `GET /bieres/[id]` --> Détail d'une bière
    - `POST /bieres` --> Création d'une bière
    - `DELETE /bieres/[id]` --> Suppression d'une bière

### Phase 2

- Création d'un formulaire de saisie de bières
- Création d'un écran listant les bières
- Création de la fonctionnalité de suppression de bières avec un écran

### Phase 3

- Création de la fonctionnalité de modification

## Qualité du code

SonarQube est utilisé pour maintenir une qualité du code source :

![Qualité du code](./sonar.png)

## Auteur

<table>
   <tr>
      <td>
         <a href="https://github.com/fridezlucas"><img width=140px src="https://secure.gravatar.com/avatar/72c1469bf815bd4e0a858341571d5111?s=800&d=identicon"><br>
         Fridez Lucas</a>
      </td>
   </tr>
</table>